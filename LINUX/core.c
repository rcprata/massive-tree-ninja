#include "core.h"

void setup (char * arg1, char * arg2)
{
	file_1 = arg1;
	file_2 = arg2;
}

void start ()
{

	initializeTree (&mainTree, &auxTree);
	
	buildAuxTree (&auxTree, file_1);
	
	buildMainTree (&mainTree, &auxTree, file_2);
	printf("%s", MESSAGE_TITLE);
	readMainTree (&mainTree);

}