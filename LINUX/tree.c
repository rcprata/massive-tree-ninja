#include "tree.h"


void initializeTree (MainTree ** mainTree, AuxTree ** auxTree)
{
	*mainTree = NULL;
	*auxTree = NULL;
}


void insertAuxTree (AuxTree ** auxTree, char * word)
{
	if (*auxTree != NULL)
	{
		if (strcmp(touppercase((*auxTree)->word), touppercase(word)) > 0)
		{
			insertAuxTree(&(*auxTree)->left, word);
		}
		else
		{
			insertAuxTree(&(*auxTree)->right, word);
		}
	}
	else
	{
		word = substring(word, 0, strlen(word) - 2);
		if (word[strlen(word) - 1] == ' ' || word[strlen(word) - 1] == '\t' || word[strlen(word) - 1] == '\n' || word[strlen(word) - 1] == '	')
			word = substring(word, 0, strlen(word) - 2);

		word = regexp(word,"[a-zA-Z!$* \\-]+");
		if (word != NULL)
		{
			*auxTree = (AuxTree *) malloc (sizeof(AuxTree));
			(*auxTree)->word = word;
			(*auxTree)->right = NULL;
			(*auxTree)->left = NULL;
		}
	}	
}

int buildAuxTree (AuxTree ** auxTree, const char * filename)
{

	char buffer[200];

	FILE * pFile = fopen(filename, ACCESS_TYPE);
	
	if (!pFile)
	{
		perror (ERROR_OPEN);
		return ERROR;
	}
	
	while (fgets(buffer, 200, pFile) != NULL)
	{
		insertAuxTree(&(*auxTree), buffer);
	}

	fclose(pFile);

    return SUCCESS;
}

void readAuxTree (AuxTree ** auxTree)
{
	if (*auxTree != NULL)
	{
		readAuxTree(&(*auxTree)->left);	
		printf("%s\n", (*auxTree)->word);
		readAuxTree(&(*auxTree)->right);
	}
}

int searchFalseWord (AuxTree ** auxTree, char * word)
{
	if (*auxTree != NULL)
	{
		if (strcmp(touppercase((*auxTree)->word), touppercase(word)) == 0)
		{
			return 1;
		}
		else
		{
			if (strcmp(touppercase((*auxTree)->word), touppercase(word)) > 0)
			{
				searchFalseWord (&(*auxTree)->left, word);
			}
			else
			{
				searchFalseWord (&(*auxTree)->right, word);
			}
		}
	}
	else
	{
		return 0;
	}
}

int searchSameWord (MainTree ** mainTree, char * word)
{
	if (*mainTree != NULL)
	{
		if (strcmp(touppercase((*mainTree)->word), touppercase(word)) == 0)
		{
			(*mainTree)->occurrence++;
			return 1;
		}
		else
		{
			if (strcmp(touppercase((*mainTree)->word), touppercase(word)) > 0)
			{
				searchSameWord (&(*mainTree)->left, word);
			}
			else
			{
				searchSameWord (&(*mainTree)->right, word);
			}
		}
	}
	else
	{
		return 0;
	}
}

void insertMainTree (MainTree ** mainTree, AuxTree ** auxTree, char * word)
{
	if (!(searchFalseWord(&(*auxTree), word) || searchSameWord(&(*mainTree), word)))
	{
		if (*mainTree != NULL)
		{
			if (strcmp(touppercase((*mainTree)->word), touppercase(word)) > 0)
			{
				insertMainTree(&(*mainTree)->left, &(*auxTree), word);
			}
			else
			{
				insertMainTree(&(*mainTree)->right, &(*auxTree), word);
			}
		}
		else
		{
			*mainTree = (MainTree *) malloc (sizeof(MainTree));
			(*mainTree)->word = word;
			(*mainTree)->occurrence = 1;
			(*mainTree)->right = NULL;
			(*mainTree)->left = NULL;
		}	
	}
}

int buildMainTree (MainTree ** mainTree, AuxTree ** auxTree, const char * filename)
{
	char buffer[200];
	char * word;

	FILE * pFile = fopen(filename, ACCESS_TYPE);
	
	if (!pFile)
	{
		perror (ERROR_OPEN);
		return ERROR;
	}
	
	while (!feof(pFile))
 	{
 		fscanf(pFile,"%s",buffer);
		word = buffer;

 		//filter only words
 		if (word[strlen(word) - 1] == ',' || word[strlen(word) - 1] == ';' || word[strlen(word) - 1] == ':' 
 			|| word[strlen(word) - 1] == '.' || word[strlen(word) - 1] == '!' || word[strlen(word) - 1] == '?'
 			|| word[strlen(word) - 1] == ')')
			
			word = substring(word, 0, strlen(word) - 1);

		if (word[0] == '[')
		{
			word = substring(word, 1, strlen(word) - 1);
		}
		
		word = regexp(word,"[a-zA-Z!$* \\-]+");

		if (word != NULL)
		{
			if ((((word[0] >= 'A') && (word[0] <= 'Z')) || ((word[0] >= 'a') && (word[0] <= 'z'))))
			{
				insertMainTree(&(*mainTree), &(*auxTree), word);
			}
			else
			{
				if (!(word[0] >= '0' && word[0] < '9'))
				{
					word = substring(word, 1, strlen(word) - 1);
					insertMainTree(&(*mainTree), &(*auxTree), word);
				}
			}
		}	

 	}
	
	fclose(pFile);

    return SUCCESS;
}

void readMainTree (MainTree ** mainTree)
{
	total++;
	if (*mainTree != NULL)
	{
		readMainTree(&(*mainTree)->left);	
		printf("%s -- %d\n", touppercase((*mainTree)->word), (*mainTree)->occurrence);
		readMainTree(&(*mainTree)->right);
	}
}

