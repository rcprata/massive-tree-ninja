#ifndef __TREE_H__
#define __TREE_H__

#include "utils.h"

typedef struct MainTree_st MainTree;	
typedef struct AuxTree_st AuxTree;

struct MainTree_st 
{
	char * word;
	int occurrence;
	MainTree * left;
	MainTree * right;
};

struct AuxTree_st
{
	char * word;
	AuxTree * left;
	AuxTree * right;
};

MainTree * mainTree;
AuxTree * auxTree;

char strArray[2000][200];

static int countArray = 0;

void initializeTree (MainTree ** mainTree, AuxTree ** auxTree);
int buildAuxTree (AuxTree ** auxTree, const char * filename);
void insertAuxTree (AuxTree ** auxTree, char * word);
void readAuxTree (AuxTree ** auxTree);
int searchFalseWord (AuxTree ** auxTree, char * word);
int searchSameWord (MainTree ** mainTree, char * word);
void insertMainTree (MainTree ** mainTree, AuxTree ** auxTree, char * word);
int buildMainTree (MainTree ** mainTree, AuxTree ** auxTree, const char * filename);

#endif
