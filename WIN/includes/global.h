#ifndef __GLOBAL_H__
#define __GLOBAL_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define SUCCESS 1
#define ERROR 0

// static const char * AUX_FILE = "setup.txt";
static const char * ACCESS_TYPE = "r";

static const char * ERROR_OPEN = "Error opening file";
static const char * ERROR_COMPARE = "Error comparing files";
static const char * MESSAGE_TITLE = "Palavra -- Ocorrencia\n";

static int total = 0;

#endif
