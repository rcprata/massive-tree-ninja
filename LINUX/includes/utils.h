#ifndef __UTILS_H__
#define __UTILS_H__

#include "global.h"

char * touppercase (char * str);
char * substring (const char * str, size_t begin, size_t len);
char * regexp (char *string, char *patrn);

#endif