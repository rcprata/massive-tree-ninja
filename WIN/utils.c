#include "includes/utils.h"

char * touppercase (char * str)
{
	char * newstr, * p;
    p = newstr = strdup (str);

    while(*p++ = toupper (*p));

    return newstr;
}


char * substring (const char * str, size_t begin, size_t len)
{
    if (str == 0 || strlen(str) == 0 || strlen(str) < begin || strlen(str) < (begin+len))
        return "";

    return strndup(str + begin, len);
}

char * strndup (const char *s, size_t n)
{
  char *result;
  size_t len = strlen (s);

  if (n < len)
    len = n;

  result = (char *) malloc (len + 1);
  if (!result)
    return 0;

  result[len] = '\0';
  return (char *) memcpy (result, s, len);
}
