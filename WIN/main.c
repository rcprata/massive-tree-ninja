#include <stdio.h>
#include <stdlib.h>

#include "includes/core.h"

int main (int *argc, char ** argv)
{

	setup(argv[1], argv[2]);

	start();

	return 0;
}
