#include "utils.h"

char * touppercase (char * str)
{
	char * newstr, * p;
    p = newstr = strdup (str);

    while(*p++ = toupper (*p));

    return newstr;
}


char * substring (const char * str, size_t begin, size_t len) 
{ 
  if (str == 0 || strlen(str) == 0 || strlen(str) < begin || strlen(str) < (begin+len)) 
    return 0; 

  return strndup(str + begin, len); 
} 

char * regexp (char *string, char *patrn)
{
	int i, w=0, len;
	int end, begin;
	char *word = NULL;
	regex_t rgT;
	regmatch_t match;
	regcomp(&rgT,patrn,REG_EXTENDED);
	if ((regexec(&rgT,string,1,&match,0)) == 0)
	{
		begin = (int)match.rm_so;
		end = (int)match.rm_eo;
		len = end - begin;
		word=malloc(len+1);
		for (i = begin; i < end; i++) 
		{
			word[w] = string[i];
			w++;
		}
		word[w]=0;
	}
	regfree(&rgT);
	return word;
}